﻿using System.Windows;

namespace EfApp
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            //StyleManager.ApplicationTheme = new MaterialTheme();

            Bootstrap bootstrap = new Bootstrap();
            bootstrap.Run();

            base.OnStartup(e);
        }
    }
}
