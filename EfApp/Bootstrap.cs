﻿using System.Windows;
using EfApp.ViewModels;
using EfApp.Views;
using EfApp.Views.Pages;
using EfDb.Model.Implimintation;
using EfDb.Model.Interfaces;
using GalaSoft.MvvmLight.Ioc;
using HibernatingRhinos.Profiler.Appender.EntityFramework;

namespace EfApp
{
    public class Bootstrap
    {

        public Bootstrap()
        {
        }

        public bool Run()
        {
            RegisterComponents();
            InitViews();
            CreateMainView();
            InitShell();

            EntityFrameworkProfiler.Initialize();
            return true;
        }

        private void RegisterComponents()
        {
            SimpleIoc.Default.Register<IUserControll, UserControll>();
        }

        private void CreateMainView()
        {
            SimpleIoc.Default.Register<ShellView>();
            SimpleIoc.Default.Register<ShellViewModel>();
        }

        private void InitViews()
        {
            SimpleIoc.Default.Register<HumanManagementView>();
            SimpleIoc.Default.Register<WorkerManagementView>();
            SimpleIoc.Default.Register<ModuleManagementView>();

            SimpleIoc.Default.Register<HumanManagementViewModel>();
            SimpleIoc.Default.Register<WorkerManagementViewModel>();
            SimpleIoc.Default.Register<ModuleManagementViewModel>();
        }

        private void InitShell()
        {
            var shell = SimpleIoc.Default.GetInstance<ShellView>();
            Application.Current.MainWindow = shell;
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            Application.Current.MainWindow?.Show();
        }
    }
}