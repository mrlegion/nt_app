﻿using GalaSoft.MvvmLight.Views;

namespace EfApp.Helpers
{
    public class NavigationService : INavigationService
    {
        public void GoBack()
        {
            throw new System.NotImplementedException();
        }

        public void NavigateTo(string pageKey)
        {
            throw new System.NotImplementedException();
        }

        public void NavigateTo(string pageKey, object parameter)
        {
            throw new System.NotImplementedException();
        }

        public string CurrentPageKey { get; }
    }
}