﻿using System;
using System.Globalization;
using System.Text;
using System.Windows.Data;

namespace EfApp.Helpers
{
    public class StringToPhoneConverter : IValueConverter
    {
        private string oldValue = "";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Test(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Test(value);
        }

        private object Test(object value)
        {
            if (value == null)
                return string.Empty;

            var phone = value.ToString().Replace("+7", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(" ", string.Empty).Replace("-", string.Empty);

            if (phone.Length > 10)
                return this.oldValue;

            return FormatNumber10D(phone);
        }

        private string FormatNumber10D(string number)
        {
            StringBuilder sb = new StringBuilder();

            if (number.Length > 0) sb.Append("+7 (");

            if (number.Length > 0) sb.Append(number.Substring(0, 1));
            if (number.Length > 1) sb.Append(number.Substring(1, 1));
            if (number.Length > 2) sb.Append(number.Substring(2, 1));

            if (number.Length > 3) sb.Append(") ");

            if (number.Length > 3) sb.Append(number.Substring(3, 1));
            if (number.Length > 4) sb.Append(number.Substring(4, 1));

            if (number.Length > 5) sb.Append("-");

            if (number.Length > 5) sb.Append(number.Substring(5, 1));
            if (number.Length > 6) sb.Append(number.Substring(6, 1));

            if (number.Length > 7) sb.Append("-");

            if (number.Length > 7) sb.Append(number.Substring(7, 1));
            if (number.Length > 8) sb.Append(number.Substring(8, 1));
            if (number.Length > 9) sb.Append(number.Substring(9, 1));

            this.oldValue = sb.ToString();

            return this.oldValue;
        }
    }
}