﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using EfApp.Views.Components;
using EfDb;
using EfDb.Model.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using MaterialDesignThemes.Wpf;

namespace EfApp.ViewModels
{
    public class HumanManagementViewModel : ViewModelBase
    {
        private readonly IUserControll userControll;
        private List<Human> humans;

        private string firstname;
        private string lastname;
        private string phone;

        private Human selectItem;

        public HumanManagementViewModel(IUserControll userControll)
        {
            this.userControll = userControll;
        }

        public RelayCommand RemoveHumanCommand
        {
            get { return new RelayCommand(() => this.userControll.Remove(SelectItem), () => SelectItem != null); }
        }

        public ReadOnlyObservableCollection<Human> Humans => this.userControll.Humans;

        public string Firstname
        {
            get => this.firstname;
            set => Set(() => Firstname, ref this.firstname, value);
        }

        public string Lastname
        {
            get => this.lastname;
            set => Set(() => Lastname, ref this.lastname, value);
        }

        public string Phone
        {
            get => this.phone;
            set => Set(() => Phone, ref this.phone, value);
        }

        public Human SelectItem
        {
            get => this.selectItem;
            set => Set(() => SelectItem, ref this.selectItem, value);
        }

        public RelayCommand RunAddHumanCommand
        {
            get { return new RelayCommand(ExecuteShowAddHumanDialog); }
        }

        private async void ExecuteShowAddHumanDialog()
        {
            var view = new AddHumanDialog {DataContext = this};
            var result = await DialogHost.Show(view, "RootDialog");

            if ((bool) result)
            {
                if (!string.IsNullOrWhiteSpace(Firstname) &&
                    !string.IsNullOrWhiteSpace(Lastname))
                {
                    this.userControll.AddHuman(Firstname, Lastname, Phone);
                    ClearFields();
                }
            }
        }

        private void ClearFields()
        {
            Firstname = Lastname = Phone = "";
        }
    }
}