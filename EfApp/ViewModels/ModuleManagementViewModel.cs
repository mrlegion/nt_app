﻿using System.Collections.ObjectModel;
using System.Windows;
using EfDb;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace EfApp.ViewModels
{
    public class ModuleManagementViewModel : ViewModelBase
    {
        private string name;
        private string alies;

        private ObservableCollection<Module> modules;

        public ModuleManagementViewModel()
        {
            this.modules = new ObservableCollection<Module>();
        }

        public ReadOnlyObservableCollection<Module> Modules
        {
            get => new ReadOnlyObservableCollection<Module>(this.modules);
        }

        public string Name
        {
            get => this.name;
            set => Set(() => Name, ref this.name, value);
        }

        public string Alies
        {
            get => this.alies;
            set => Set(() => Alies, ref this.alies, value);
        }

        public RelayCommand CreateModuleCommand => new RelayCommand(() =>
        {
            if (!string.IsNullOrWhiteSpace(Name) &&
                !string.IsNullOrWhiteSpace(Alies))
            {
                this.modules.Add(new Module() {Name = this.Name, Alies = this.Alies});
            }
        }, () => !string.IsNullOrWhiteSpace(Name) && !string.IsNullOrWhiteSpace(Alies));
    }
}