﻿using System.Windows;
using System.Windows.Controls;
using EfApp.Views;
using EfApp.Views.Pages;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Ioc;

namespace EfApp.ViewModels
{
    public enum Windows
    {
        Shell = 0,
        Human = 1,
        Worker = 2,
        Material = 3,
        Authorization = 4,
        Modules = 5
    }

    public class ShellViewModel : ViewModelBase
    {
        private string count;
        private Window window;
        private Page content;

        public string Count
        {
            get => this.count;
            set => Set(() => Count, ref this.count, value);
        }

        public ShellViewModel()
        {
        }

        public RelayCommand ExitCommand => new RelayCommand(() => { Application.Current.Shutdown(); });

        public RelayCommand LoadHumanViewCommand => new RelayCommand(() => ShowWindow(Windows.Human));

        public RelayCommand LoadWorkerViewCommand => new RelayCommand(() => ShowWindow(Windows.Worker));

        public RelayCommand LoadModuleViewCommand => new RelayCommand(() => ShowWindow(Windows.Modules));

        public RelayCommand LoginCommand => new RelayCommand(() => {});

        public Page Content
        {
            get => this.content;
            set => Set(() => Content, ref this.content, value);
        }

        private void ShowWindow(Windows type)
        {
            switch (type)
            {
                case Windows.Human:
                    Content =  SimpleIoc.Default.GetInstance<HumanManagementView>();
                    break;
                case Windows.Worker:
                    Content = SimpleIoc.Default.GetInstance<WorkerManagementView>();
                    break;
                case Windows.Modules:
                    break;
                default:
                    break;
            }
        }
    }
}