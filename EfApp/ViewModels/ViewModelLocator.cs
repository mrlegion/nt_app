﻿
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace EfApp.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {

            if (ViewModelBase.IsInDesignModeStatic)
            {
            }
            else
            {
                
            }
        }

        public ShellViewModel Shell
        {
            get { return SimpleIoc.Default.GetInstance<ShellViewModel>(); }
        }

        public WorkerManagementViewModel Worker
        {
            get { return SimpleIoc.Default.GetInstance<WorkerManagementViewModel>(); }
        }

        public HumanManagementViewModel Human
        {
            get { return SimpleIoc.Default.GetInstance<HumanManagementViewModel>(); }
        }

        public ModuleManagementViewModel Module
        {
            get { return SimpleIoc.Default.GetInstance<ModuleManagementViewModel>(); }
        }
    }
}