﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using EfApp.Views;
using EfDb;
using EfDb.Model.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Ioc;

namespace EfApp.ViewModels
{
    public class WorkerManagementViewModel : ViewModelBase
    {
        private readonly IUserControll userControll;

        public WorkerManagementViewModel(IUserControll userControll)
        {
            this.userControll = userControll;
        }

        public List<Worker> Workers => this.userControll.GetAllWorkers();
    }
}