﻿using System.Text;
using System.Windows.Controls;

namespace EfApp.Views.Components
{
    public enum TextBoxMask
    {
        Phone10D
    }

    public class MaskedTextBox : TextBox
    {
        public TextBoxMask Mask { get; set; }

        public MaskedTextBox()
        {
            this.TextChanged += new TextChangedEventHandler(MaskedTextBoxTextChanged);
        }

        private void MaskedTextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            this.CaretIndex = this.Text.Length;

            if (sender is MaskedTextBox tbEntry && tbEntry.Text.Length > 0)
            {
                tbEntry.Text = FormatNumber(tbEntry.Text, tbEntry.Mask);
            }
        }

        private string FormatNumber(string text, TextBoxMask mask)
        {
            int x;
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();

            if (text != null)
            {
                for (int i = 0; i < text.Length; i++)
                {
                    if (int.TryParse(text.Substring(i, 1), out x))
                    {
                        sb.Append(x.ToString());
                    }
                }
                switch (mask)
                {
                    case TextBoxMask.Phone10D:
                        return FormatFor10D(sb.ToString());

                    default:
                        break;
                }

            }

            return sb.ToString();
        }

        private string FormatFor10D(string sb)
        {
            StringBuilder sb2 = new StringBuilder();

            if (sb.Length > 0) sb2.Append("(");

            if (sb.Length > 0) sb2.Append(sb.Substring(0, 1));
            if (sb.Length > 1) sb2.Append(sb.Substring(1, 1));
            if (sb.Length > 2) sb2.Append(sb.Substring(2, 1));

            if (sb.Length > 3) sb2.Append(") ");

            if (sb.Length > 3) sb2.Append(sb.Substring(3, 1));
            if (sb.Length > 4) sb2.Append(sb.Substring(4, 1));
            if (sb.Length > 5) sb2.Append(sb.Substring(5, 1));

            if (sb.Length > 6) sb2.Append("-");

            if (sb.Length > 6) sb2.Append(sb.Substring(6, 1));
            if (sb.Length > 7) sb2.Append(sb.Substring(7, 1));
            if (sb.Length > 8) sb2.Append(sb.Substring(8, 1));
            if (sb.Length > 9) sb2.Append(sb.Substring(9, 1));

            return sb2.ToString();
        }
    }
}