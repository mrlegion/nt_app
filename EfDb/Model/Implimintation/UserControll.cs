﻿using EfDb.Model.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace EfDb.Model.Implimintation
{
    public class UserControll : IUserControll
    {
        private readonly DbNtContext context;
        private ObservableCollection<Human> humans;

        public UserControll()
        {
            context = new DbNtContext();
        }

        public ReadOnlyObservableCollection<Human> Humans
        {
            get
            {
                if (humans == null) humans = new ObservableCollection<Human>(context.Human);
                return new ReadOnlyObservableCollection<Human>(humans);
            }
        }

        public List<Human> GetAllHumans()
        {
            return context.Human.ToList();
        }

        public void AddHuman(string firstname, string lastname, string phone)
        {
            if (string.IsNullOrWhiteSpace(phone))
                phone = "+7 900 00-00-000";

            context.Human.Add(new Human { Firstname = firstname, Lastname = lastname, Phone = phone });
            context.SaveChanges();
            humans = new ObservableCollection<Human>(context.Human);
        }

        public void Remove(Human human)
        {
            if (human == null)
                return;

            context.Human.Remove(human);
            context.SaveChanges();

            humans.Remove(human);
        }

        public List<Worker> GetAllWorkers()
        {
            var result = from worker in this.context.Worker
                         join human in this.context.Human on worker.HumanId equals human.Id
                         select new
                         {
                             Firstname = human.Firstname,
                             Lastname = human.Lastname,
                             Phone = human.Phone,
                             Password = worker.Password
                         };
        }
    }
}