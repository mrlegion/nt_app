﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace EfDb.Model.Interfaces
{
    public interface IUserControll
    {
        List<Human> GetAllHumans();
        List<Worker> GetAllWorkers();
        ReadOnlyObservableCollection<Human> Humans { get; }
        void AddHuman(string firstname, string lastname, string phone);
        void Remove(Human human);
    }
}