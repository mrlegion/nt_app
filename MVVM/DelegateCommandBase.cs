﻿using System;
using System.Windows.Input;

namespace MVVM
{
    public abstract class DelegateCommandBase : ICommand
    {
        protected abstract void Execute(object parameter);

        protected abstract bool CanExecute(object parameter);

        public virtual event EventHandler CanExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            OnCanExecuteChanged();
        }

        protected virtual void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        void ICommand.Execute(object parameter)
        {
            Execute(parameter);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute(parameter);
        }

    }
}